import time
from d3m import index, utils

from D3MPrimitiveProfiler.TestClassificationPrimitive import TestPrimitive, pretty_print


if __name__ == '__main__':
    primitive = index.get_primitive('d3m.primitives.classification.decision_tree.SKlearn',)
    start_time = time.time()
    _time = time.time()
    r = TestPrimitive(primitive=primitive)
    print('Init Done', time.time() - _time)
    _time = time.time()
    with utils.silence():
        r.basic_test()
    print('Basic Test Done', time.time() - _time)
    _time = time.time()
    with utils.silence():
        r.scalability_test()
    print('Scalability Test Done', time.time() - _time)
    _time = time.time()
    r.hyperparameter_importance()
    print('Hyperparameter Importance Test Done', time.time() - _time)
    _time = time.time()
    pretty_print(r.attributes)
    print('Total_test_time: ', time.time() - start_time)