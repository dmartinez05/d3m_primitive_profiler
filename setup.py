import os
import sys
from setuptools import setup, find_packages

PACKAGE_NAME = 'D3MPrimitiveProfiler'
MINIMUM_PYTHON_VERSION = 3, 6


def check_python_version():
    """Exit when the Python version is too low."""
    if sys.version_info < MINIMUM_PYTHON_VERSION:
        sys.exit("Python {}.{}+ is required.".format(*MINIMUM_PYTHON_VERSION))


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    raise KeyError("'{0}' not found in '{1}'".format(key, module_path))


def read_readme():
    with open(os.path.join(os.path.dirname(__file__), 'README.md'), encoding='utf8') as file:
        return file.read()


def read_entry_points():
    with open('entry_points.ini') as entry_points:
        return entry_points.read()


check_python_version()
version = read_package_variable('__version__')

setup(
    name=PACKAGE_NAME,
    version=version,
    description='D3M TAMU primitives',
    author='Diego Martinez',
    packages=find_packages(exclude=['examples']),
    data_files=['./'],
    install_requires=[
        'd3m',
        'numpy>=1.15.4,<=1.17.3',
        'pandas>=0.23.4,<=0.25.2',
        'memory_profiler',
        'scikit-learn[alldeps]>=0.20.3,<=0.21.3',
    ],
    # entry_points=read_entry_points(),
    url='https://gitlab.com/TAMU_D3M/d3m_primitives',
    license='Apache-2.0',
    classifiers=[
          'License :: OSI Approved :: Apache Software License',
    ],
)

