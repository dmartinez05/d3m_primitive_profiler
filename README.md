# D3MPrimitiveProfiler

This tool was developed for testing, validating, and profiling some of the 
standard functionality expected on primitives wrapped/created for 
[D3M](https://gitlab.com/datadrivendiscovery/d3m/)

## Installation
To install the package, please use the pip installation as follows:
```
pip3 install -e git+https://gitlab.com/dmartinez05/d3m_primitive_profiler.git@master#egg=D3MPrimitiveProfiler
```

## Example

``` python
from d3m import index

from D3MPrimitiveProfiler.TestClassificationPrimitive import TestPrimitive, pretty_print

primitive = index.get_primitive('d3m.primitives.classification.decision_tree.SKlearn',)

test = TestPrimitive(primitive=primitive)
test.basic_test()
test.scalability_test()
test.hyperparameter_importance()
pretty_print(test.attributes['summary'])
```

## Notes

In order to test multiple primitives, you can find them at [primitives](https://gitlab.com/datadrivendiscovery/primitives).
For simplicity you can find a docker image that contains all the things necessary to test it at [images](https://gitlab.com/datadrivendiscovery/images).

The image that we use for testing is 
[registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2019.11.10-20191127-050901](registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2019.11.10-20191127-050901).
After pulling the image, it is necessary to install our tool.
