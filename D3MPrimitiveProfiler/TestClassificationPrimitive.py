import itertools
import json
import random
import statistics
import sys
import time
from pprint import pprint

import numpy
import pandas as pd
from d3m import index, utils
from d3m.container import DataFrame as D3M_Dataframe
from d3m.metadata import base as metadata_base, problem
from d3m.metadata.hyperparams import UniformInt, Enumeration
from memory_profiler import memory_usage
from sklearn.datasets import make_classification
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

LENGTH = 80


Data_types = {
    'int': 'http://schema.org/Integer',
    'float': 'http://schema.org/Float',
    'categorical': 'https://metadata.datadrivendiscovery.org/types/CategoricalData',
    'target': 'https://metadata.datadrivendiscovery.org/types/TrueTarget',
    'attribute': 'https://metadata.datadrivendiscovery.org/types/Attribute'
}


def pretty_print(obj):
    print(json.dumps(obj, sort_keys=True, indent=4))


def mb_to_gb(mb):
    gb = mb / 1024
    return gb


def bytes_to_gb(b):
    kb = b / 1024
    mb = kb / 1024
    gb = mb / 1024
    return gb


def get_size_in_gb(obj):
    return bytes_to_gb(sys.getsizeof(obj))


def linear_error(x, y):
    regression = LinearRegression()
    regression.fit(x, y)
    pred = regression.predict(x)
    return mean_squared_error(y, pred)


def sample_hp_make_classification():
    # binary classification so just 2 classes
    n_classes = 2
    n_samples = random.choice(range(100, 25000))
    n_features = random.choice(range(1, 10000))
    # Number of informative, redundant and repeated features must sum to less than the number of total features
    n_informative = random.choice(range(0, n_features))
    n_redundant = random.choice(range(0, n_features - n_informative))
    n_repeated = random.choice(range(0, n_features - n_informative - n_redundant))

    hypercube = random.choice([True, False])

    # n_classes * n_clusters_per_class must be smaller or equal 2 ** n_informative
    # then n_clusters_per_class <= (2 ** n_informative)/ n_classes
    # bit this is not doable because often goes beyond pow(2, 100) and everything crash
    _n_clusters = 400
    if n_informative < _n_clusters:
        _n_clusters = n_informative
    n_clusters_per_class = int(random.choice(range(0, _n_clusters)))

    flip_y = random.choice([x * 0.01 for x in range(0, 100)] + [1.0])
    class_sep = random.choice([x * 0.01 for x in range(0, 100)] + [1.0])

    hp_config = {
        'n_classes': n_classes,
        'n_samples': n_samples,
        'n_features': n_features,
        'n_informative': n_informative,
        'n_redundant': n_redundant,
        'n_repeated': n_repeated,
        'hypercube': hypercube,
        'n_clusters_per_class': n_clusters_per_class,
        'flip_y': flip_y,
        'class_sep': class_sep,
    }
    return hp_config


def sample_hp_make_classification_size(samples=None, features=None):
    # binary classification so just 2 classes
    n_classes = 2
    samples_size = {
        'small': 100,
        'medium': 12500,
        'large': 25000
    }

    features_size = {
        'small': 1,
        'medium': 5000,
        'large': 10000
    }

    if samples is None:
        n_samples = random.choice(range(100, 25000))
    else:
        n_samples = samples_size[samples]

    if features is None:
        n_features = random.choice(range(1, 10000))
    else:
        n_features = features_size[features]

    hp_config = {
        'n_classes': n_classes,
        'n_samples': n_samples,
        'n_features': n_features,
    }
    return hp_config


def make_random_dataset():
    hp = sample_hp_make_classification()
    dataset = make_classification(**hp)
    return hp, dataset


def add_index(df):
    new_df = df
    new_df.insert(0, 'd3mIndex', range(len(new_df)))

    new_metadata = new_df.metadata
    new_metadata = new_metadata.update((metadata_base.ALL_ELEMENTS, 0), {
        'name': 'd3mIndex',
        'structural_type': numpy.int64,
        'semantic_types': [
            'http://schema.org/Integer',
            'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
        ],
    })

    new_df.metadata = new_metadata
    return new_df


def fix_y(df):
    y = D3M_Dataframe(data=df.iloc[:, -1], columns=['target'])
    y = add_index(y)
    return y


def generate_metadata(df, target_columns=[], categorical_targets=False, target_index=None, use_index=False):
    # init metadata
    metadata = metadata_base.DataMetadata()
    metadata = metadata.update((), {
        'structural_type': type(df),
        'semantic_types': [
            'https://metadata.datadrivendiscovery.org/types/Table',
        ],
        'dimension': {
            'name': 'rows',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
            'length': len(df),
        },
    })

    num_columns = len(df.columns)
    # if not use_index:
    #     num_columns -= 1
    metadata = metadata.update((metadata_base.ALL_ELEMENTS,), {
        'dimension': {
            'name': 'columns',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
            'length': num_columns,
        }
    })

    for i, column_name in enumerate(df.columns):
        if i == 0:
            if use_index:
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, i), {
                    'name': column_name,
                    'structural_type': numpy.int64,
                    'semantic_types': [
                        'http://schema.org/Integer',
                        'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
                    ],
                })
            else:
                is_target = False
                structural_type = numpy.float64
                index = i
                if column_name in target_columns:
                    is_target = True
                    if categorical_targets:
                        structural_type = str
                    else:
                        structural_type = numpy.int64

                    if target_index is not None:
                        index = target_index

                semantic_types = infer_semantuc_type(df[column_name], target=is_target)
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, index), {
                    'name': column_name,
                    'structural_type': structural_type,
                    'semantic_types': semantic_types,
                })
        else:
            is_target = False
            structural_type = numpy.float64
            index = i
            if column_name in target_columns:
                is_target = True
                if categorical_targets:
                    structural_type = str
                else:
                    structural_type = numpy.int64

                if target_index is not None:
                    index = target_index

            semantic_types = infer_semantuc_type(df[column_name], target=is_target)
            metadata = metadata.update((metadata_base.ALL_ELEMENTS, index), {
                'name': column_name,
                'structural_type': structural_type,
                'semantic_types': semantic_types,
            })
    return metadata


# TODO Add semantic types.
def numpy_to_dataframe(attributes, targets, categorical_targets=False, use_semantic_types=False, use_index=True):

    df_attributes = pd.DataFrame(attributes)
    attributes_column_names = ['attribute_'+str(i) for i in range(len(df_attributes.columns))]
    df_attributes.columns = attributes_column_names

    df_targets = pd.DataFrame(targets)
    targets_column_names = ['target']
    df_targets.columns = targets_column_names
    if categorical_targets:
        df_targets = df_targets.astype(str)

    # add index to the attributes
    if use_index:
        df_attributes.insert(0, 'd3mIndex', range(len(df_attributes)))

    if use_semantic_types:
        # print('#' * 50, ' use_semantic_types', '#' * 50)
        df_dataset = pd.concat([df_attributes, df_targets], axis=1)
        df_dataset = D3M_Dataframe(df_dataset, generate_metadata=True)

        df_dataset.metadata = generate_metadata(df_dataset, df_targets.columns,
                                                categorical_targets=categorical_targets, use_index=use_index)
        # df_dataset.metadata.pretty_print()
        return df_dataset, df_dataset

    else:
        # print('#' * 50, ' attributes')
        df_attributes = D3M_Dataframe(df_attributes, generate_metadata=True)
        df_attributes.metadata = generate_metadata(df_attributes, use_index=use_index)
        # df_attributes.metadata.pretty_print()

        # print('#' * 50, ' targets')
        target_index = len(df_attributes.columns)
        # add index to the attributes
        if use_index:
            df_targets.insert(0, 'd3mIndex', range(len(df_targets)))
        df_targets = D3M_Dataframe(df_targets, generate_metadata=True)
        df_targets.metadata = generate_metadata(df_targets, target_columns=df_targets.columns,
                                                categorical_targets=categorical_targets, target_index=target_index, use_index=use_index)
        # df_targets.metadata.pretty_print()
        return df_attributes, df_targets


# for now we just throw float for testing
def infer_semantuc_type(dataframe_column, target):
    semantic_types = []
    inferred_type = pd.api.types.infer_dtype(dataframe_column)

    if inferred_type == 'floating':
        _type = 'float'
    elif inferred_type == 'integer':
        _type = 'int'
    elif inferred_type == 'string':
        _type = 'categorical'
    else:
        raise ValueError('Type {} not supported'.format(inferred_type))

    if target:
        semantic_types.append(Data_types['target'])
    else:
        semantic_types.append(Data_types['attribute'])

    semantic_types.append(Data_types[_type])
    # print(semantic_types)

    return semantic_types


# TODO add data ways to generate
def create_dataset(categorial_targets=False, use_semantic_types=False, use_index=True):
    x, y = make_classification()
    X, Y = numpy_to_dataframe(x, y, categorical_targets=categorial_targets, use_semantic_types=use_semantic_types, use_index=use_index)
    return X, Y


def make_splitted_dataset(test_size=0.30, categorial_targets=False, use_semantic_types=False, use_index=True, dataset_params={}):
    x, y = make_classification(**dataset_params)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size, random_state=42)
    x_train, y_train = numpy_to_dataframe(x_train, y_train,
                                          categorical_targets=categorial_targets,
                                          use_semantic_types=use_semantic_types,
                                          use_index=use_index)

    x_test, y_test = numpy_to_dataframe(x_test, y_test,
                                          categorical_targets=categorial_targets,
                                          use_semantic_types=use_semantic_types,
                                          use_index=use_index)

    if use_semantic_types:
        x_test = x_test.drop('target', axis=1)
    return x_train, y_train, x_test, y_test


class Profiler(object):
    def __init__(self):
        self.runs = []
        self.scores = None

    # mem in mb
    def profile_function(self, function, arguments, skip_arguments=True):
        mem_snapshots, time_values = memory_usage((time_it, [function], arguments), include_children=True, retval=True)
        total_time, return_values = time_values

        profile = {
            'function_name': function.__name__,
        }
        if not skip_arguments:
            profile['arguments'] = arguments
        resource_profile = {
            'mem_snapshots': mem_snapshots,
            'max_memory': max(mem_snapshots),
            'total_time': total_time,
        }
        profile['profile'] = resource_profile

        self.runs.append(profile)
        return return_values

    def memory_summary(self):
        max_memory = []
        for run in self.runs:
            max_memory.append(run['profile']['max_memory'])
        return mb_to_gb(max(max_memory))

    def time_summary(self):
        total_time = []
        for run in self.runs:
            total_time.append(run['profile']['total_time'])
        return sum(total_time)

def time_it(func, *args, **kwargs):
    start = time.time()
    return_values = func(*args, **kwargs)
    total_time = time.time() - start
    return total_time, return_values


class PrimitiveTest:
    def __init__(self, primitive):
        self.primitive = primitive
        self.primitive_name = str(primitive)
        self.primitive_state = None

        self.is_init = False
        self.is_set_training_data = False
        self.is_fitted = False
        self.is_produced = False
        self.is_scored = False

        self.init_error = None
        self.set_training_data_error = None
        self.fit_error = None
        self.produce_error = None
        self.score_error = None

        self.profiler = Profiler()

    def clear_state(self):
        self.primitive_state = None
        self.is_init = False
        self.is_set_training_data = False
        self.is_fitted = False
        self.is_produced = False
        self.is_scored = False

        self.init_error = None
        self.set_training_data_error = None
        self.fit_error = None
        self.produce_error = None
        self.score_error = None

        self.profiler = Profiler()

    def init_primitive(self, primitive_hyperparameters):
        if self.is_init:
            raise RuntimeError('Primitive {} is already initialized.'.format(self.primitive_name))

        try:
            self.primitive_state = self.profiler.profile_function(
                self.primitive, {'hyperparams': primitive_hyperparameters})
            self.is_init = True
        except Exception as e:
            self.init_error = e

    def set_training_data(self, x, y):
        if not self.is_init:
            raise RuntimeError('Primitive {} has not been initialized.'.format(self.primitive_name))
        if self.init_error is not None:
            raise RuntimeError('Primitive {} has an init error'.format(self.primitive_name))
        if self.is_set_training_data:
            raise RuntimeError('Primitive {} has already assigned data.'.format(self.primitive_name))

        try:
            self.profiler.profile_function(self.primitive_state.set_training_data, {'inputs': x, 'outputs': y})
            self.is_set_training_data = True
        except Exception as e:
            self.set_training_data_error = e

    def fit(self):
        if not self.is_set_training_data:
            raise RuntimeError('Primitive {} has no data.'.format(self.primitive_name))
        if self.set_training_data_error is not None:
            raise RuntimeError('Primitive {} failed init data.'.format(self.primitive_name))
        if self.is_fitted:
            raise RuntimeError('Primitive {} has already been fitted.'.format(self.primitive_name))

        try:
            self.profiler.profile_function(self.primitive_state.fit, {})
            self.is_fitted = True
        except Exception as e:
            self.fit_error = e

    def produce(self, x):
        if not self.is_fitted:
            raise RuntimeError('Primitive {} is not fitted.'.format(self.primitive_name))
        if self.fit_error is not None:
            raise RuntimeError('Primitive {} failed fitting.'.format(self.primitive_name))

        predictions = None
        try:
            call_result = self.profiler.profile_function(self.primitive_state.produce, {'inputs': x})
            predictions = call_result.value
            self.is_set_training_data = True
        except Exception as e:
            self.produce_error = e

        if predictions is None:
            raise RuntimeError('Primitive {} Produce not produced any output.'.format(self.primitive_name))
        self.is_produced = True
        return predictions

    def score(self, truth, predictions, metrics=None):
        if not self.is_produced:
            raise RuntimeError('Primitive {} is not produced.'.format(self.primitive_name))
        if self.produce_error is not None:
            raise RuntimeError('Primitive {} failed producing.'.format(self.primitive_name))

        try:
            _truth = fix_y(truth)
            _predictions = fix_y(predictions)

            if metrics is None:
                metrics = [
                    {
                        'metric': 'ACCURACY'
                    },
                ]

            outputs = {
                'metric': [],
                'value': [],
                'normalized': []
            }

            for metric_configuration in metrics:
                metric = problem.PerformanceMetric[metric_configuration['metric']]
                params = {}
                for param_name, param_value in metric_configuration.items():
                    if param_name == 'metric':
                        continue
                    if param_value is None:
                        continue
                    params[param_name] = param_value
                _score = metric.get_class()(**params).score(_truth, _predictions)

                outputs['metric'].append(metric.name)
                outputs['value'].append(_score)
                outputs['normalized'].append(metric.normalize(_score))
            results = D3M_Dataframe(data=outputs, columns=list(outputs.keys()), generate_metadata=True)
            self.profiler.scores = results

        except Exception as e:
            self.produce_error = e
        return

    def run_test(self, primitive_hyperparameters, x, y, x_test, y_test, metrics=None):
        self.init_primitive(primitive_hyperparameters)
        if self.init_error:
            print('init_error', self.init_error)
            return

        self.set_training_data(x, y)
        if self.set_training_data_error:
            print('set_training_data_error', self.set_training_data_error)
            return

        self.fit()
        if self.fit_error:
            print('fit_error', self.fit_error)
            return

        predictions = self.produce(x_test)
        if self.produce_error:
            print('produce_error', self.produce_error)
            return

        self.score(y_test, predictions, metrics)
        return

    def pretty_print_report(self):
        pprint(self.profiler.runs)

        if self.init_error:
            print('init_error', self.init_error)
        elif self.set_training_data_error:
            print('set_training_data_error', self.set_training_data_error)
        elif self.fit_error:
            print('fit_error', self.fit_error)
        elif self.produce_error:
            print('produce_error', self.produce_error)
        elif self.score_error:
            print('score_error', self.score_error)
        print(self.profiler.scores)

    def get_error(self):
        if self.init_error:
            return 'init_error ' + str(self.init_error)
        elif self.set_training_data_error:
            return 'set_training_data_error ' + str(self.set_training_data_error)
        elif self.fit_error:
            return 'fit_error ' + str(self.fit_error)
        elif self.produce_error:
            return 'produce_error ' + str(self.produce_error)
        elif self.score_error:
            return 'score_error ' + str(self.score_error)


def print_name(name):
    length = LENGTH
    free_space = length - len(name) - 2
    space = int(free_space / 2)
    name = '#' + ' ' * space + name + ' ' * space
    if free_space % 2 == 0:
        name = name + '#'
    else:
        name = name + ' #'

    print("#" * length)
    print(name)
    print("#" * length)


def print_space():
    print('-' * LENGTH)


class TestPrimitive():
    def __init__(self, *, primitive_path=None, primitive=None):
        if primitive_path is None and primitive is None:
            raise RuntimeError('No primitive has been specified')

        if primitive is not None:
            primitive_path = primitive.metadata.query()['python_path']
            self.primitive = primitive
        else:
            self.primitive = index.get_primitive(primitive_path)

        self.hyperparameter_space = self.primitive.metadata.get_hyperparams()
        print_name(primitive_path)

        self.attributes = {
            'summary': {}
        }

    def basic_test(self):
        self.check_descriptions()
        self.use_semantic_type()
        self.check_predictions_metadata()
        self.check_deterministic()

    def scalability_test(self):
        # Check if there is some information about this primitive
        if 'dataset_attributes' not in self.attributes:
            return
        elif not self.attributes['dataset_attributes']['accepted_parameters']:
            return

        params = self.attributes['dataset_attributes']['accepted_parameters'][0]
        hyperams = self.hyperparameter_space.defaults()
        if self.attributes['has_use_semantic_types']:
            hyperams = hyperams.replace({'use_semantic_types': params['use_semantic_types']})

        # feature_space = [1, 50, 100, 500, 1000, 5000, 10000]
        # samples_space = [100, 500, 1000, 5000, 10000]

        feature_space = [20, 50, 100, 500, 1000]
        samples_space = [100, 500, 1000, 5000]

        scalability_info = []
        for n_features, n_samples in itertools.product(feature_space, samples_space):
            dataset_params = {
                'n_samples': n_samples,
                'n_features': n_features,
            }
            params['dataset_params'] = dataset_params
            x_train, y_train, x_test, y_test = make_splitted_dataset(**params)
            dataset_run = {
                'dataset_params': dataset_params,
                'dataset_size': get_size_in_gb(x_train) + get_size_in_gb(y_train)
            }
            primitive_test = PrimitiveTest(self.primitive)
            with utils.silence():
                primitive_test.run_test(hyperams, x_train, y_train, x_test, y_test)

            if primitive_test.profiler.scores is None:
                dataset_run['error'] = primitive_test.get_error()
            else:
                # pprint(primitive_test.profiler.runs)
                dataset_run['max_memory'] = primitive_test.profiler.memory_summary()
                dataset_run['total_time'] = primitive_test.profiler.time_summary()
                dataset_run['normalized_score'] = primitive_test.profiler.scores['normalized'].sum() / len(primitive_test.profiler.scores)

            scalability_info.append(dataset_run)

        if scalability_info:
            # store all information
            self.attributes['scalability'] = scalability_info

            # make a summary:
            summary = {}
            sizes = []
            times = []
            memory = []
            features = []
            samples = []
            for scal in scalability_info:
                sizes.append([scal['dataset_size']])
                times.append([scal['total_time']])
                memory.append([scal['max_memory']])
                features.append([scal['dataset_params']['n_features']])
                samples.append([scal['dataset_params']['n_samples']])
            summary['linearity'] = linear_error(sizes, times)
            summary['memory_usage'] = linear_error(sizes, memory)
            features_samples = pd.concat([D3M_Dataframe(features), D3M_Dataframe(samples)], axis=1).values.tolist()
            summary['complexity'] = linear_error(features_samples, times)

            self.attributes['summary']['scalability'] = summary

    def hyperparameter_importance(self):
        if not self.attributes['dataset_attributes']['accepted_parameters']:
            print('There is no configuration for this dataset')
            return
        params = self.attributes['dataset_attributes']['accepted_parameters'][0]
        hyperams = self.hyperparameter_space.defaults()
        if self.attributes['has_use_semantic_types']:
            hyperams = hyperams.replace({'use_semantic_types': params['use_semantic_types']})
        x_train, y_train, x_test, y_test = make_splitted_dataset(**params)

        hyperparam_space = {}
        for key, value in hyperams.configuration.items():
            if 'https://metadata.datadrivendiscovery.org/types/TuningParameter' in value.semantic_types:
                if isinstance(value, UniformInt):
                    low_bound = value.lower
                    if not value.lower_inclusive:
                        low_bound += 1
                    upper_bound = 100
                    if value.upper is not None:
                        upper_bound = value.upper
                    if not value.upper_inclusive:
                        upper_bound -= 1

                    if upper_bound - low_bound >= 5:
                        step = int((upper_bound - low_bound) / 5)
                        hyperparam_space[key] = list(range(low_bound, upper_bound, step))

                if isinstance(value, Enumeration):
                    hyperparam_space[key] = value.values

        # higher is more important
        hyperparam_importance = {}
        for hyperparam, values in hyperparam_space.items():
            scores = []
            for value in values:
                primitive_test = PrimitiveTest(self.primitive)
                with utils.silence():
                    try:
                        primitive_test.run_test(hyperams.replace({hyperparam: value}), x_train, y_train, x_test, y_test)
                        score = primitive_test.profiler.scores['normalized'].sum() / len(primitive_test.profiler.scores)
                    except:
                        score = 0
                scores.append(score)
                importance = 0
                if len(scores) > 1:
                    importance = statistics.stdev(scores)
                hyperparam_importance[hyperparam] = importance
        self.attributes['hyperparameter_importance'] = hyperparam_importance
        self.attributes['summary']['hyperparameter_importance'] = hyperparam_importance

    def check_predictions_metadata(self):
        if not self.attributes['dataset_attributes']['accepted_parameters']:
            print('There is no configuration for this dataset')
            return
        params = self.attributes['dataset_attributes']['accepted_parameters'][0]
        hyperams = self.hyperparameter_space.defaults()
        if self.attributes['has_use_semantic_types']:
            hyperams = hyperams.replace({'use_semantic_types': params['use_semantic_types']})

        x_train, y_train, x_test, y_test = make_splitted_dataset(**params)
        primitive_test = PrimitiveTest(self.primitive)
        with utils.silence():
            primitive_test.init_primitive(hyperams)
            primitive_test.set_training_data(x_train, y_train)
            primitive_test.fit()
            predictions = primitive_test.produce(x_test)
        predictions_metadata_index = predictions.metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'] - 1

        if params['categorial_targets']:
            reference_metadata = {
                'structural_type': str,
                'name': 'target',
                'semantic_types': (
                    'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                    'https://metadata.datadrivendiscovery.org/types/PredictedTarget'
                )
            }
        else:
            reference_metadata = {
                'structural_type': numpy.int64,
                'name': 'target',
                'semantic_types': (
                    'https://metadata.datadrivendiscovery.org/types/Integer'
                    'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                )
            }

        predictions_metadata = predictions.metadata.query((metadata_base.ALL_ELEMENTS, predictions_metadata_index))

        metadata_is_different = False
        for key, value in reference_metadata.items():
            if predictions_metadata[key] != value:
                if isinstance(value, tuple):
                    if predictions_metadata[key] != tuple(reversed(value)):
                        metadata_is_different = True
                        break
                else:
                    metadata_is_different = True
                    break

        if metadata_is_different:
            self.attributes['correct_output_metadata'] = False
        else:
            self.attributes['correct_output_metadata'] = True

        self.attributes['summary']['correct_output_metadata'] = self.attributes['correct_output_metadata']

    def check_deterministic(self):
        # get dataset from the first best configuration we have.
        if not self.attributes['dataset_attributes']['accepted_parameters']:
            print('There is no configuration for this dataset')
            return

        scores = []
        params = self.attributes['dataset_attributes']['accepted_parameters'][0]
        hyperams = self.hyperparameter_space.defaults()
        if self.attributes['has_use_semantic_types']:
            hyperams = hyperams.replace({'use_semantic_types': params['use_semantic_types']})

        x_train, y_train, x_test, y_test = make_splitted_dataset(**params)

        # by the law of big numbers 0, 32
        for i in range(0, 3):
            primitive_test = PrimitiveTest(self.primitive)
            with utils.silence():
                primitive_test.run_test(hyperams, x_train, y_train, x_test, y_test)
            scores.append(primitive_test.profiler.scores['normalized'].sum() / len(primitive_test.profiler.scores))

        scores_std = statistics.stdev(scores)
        # change alpha for deterministic definition.
        if scores_std > 0.00001:
            self.attributes['deterministic'] = False
        else:
            self.attributes['deterministic'] = True

        self.attributes['summary']['deterministic'] = self.attributes['deterministic']

    def use_semantic_type(self):
        hyperams = self.hyperparameter_space.defaults()
        accepted_parameters = []
        rejected_parameters = []

        permutations = list(itertools.product([True, False], [True, False], [True, False]))
        for categorial_targets, use_semantic_types, use_index in permutations:
            params = {
                'use_semantic_types': use_semantic_types,
                'use_index': use_index,
                'categorial_targets': categorial_targets,
            }

            # if primitive has hyperparameter `use_semantic_types` we update it according to dataset
            if 'use_semantic_types' in self.hyperparameter_space.configuration:
                hyperams = hyperams.replace({'use_semantic_types': use_semantic_types})
            x_train, y_train, x_test, y_test = make_splitted_dataset(**params)
            primitive_test = PrimitiveTest(self.primitive)
            with utils.silence():
                primitive_test.run_test(hyperams, x_train, y_train, x_test, y_test)
            if primitive_test.profiler.scores is not None:
                accepted_parameters.append(params)
            else:
                params['error'] = primitive_test.get_error()
                rejected_parameters.append(params)

        dataset_attributes = {
            'accepted_parameters': accepted_parameters,
            'rejected_parameters': rejected_parameters,
        }

        self.attributes['dataset_attributes'] = dataset_attributes
        self.attributes['has_use_semantic_types'] = 'use_semantic_types' in self.hyperparameter_space.configuration

        common_errors = set()
        for failed in rejected_parameters:
            common_errors.add(failed['error'])

        explanations = []
        for error in common_errors:
            for fail in rejected_parameters:
                if fail['error'] == error:
                    _error = fail['error']
                    if not fail['error']:
                        _error = "No description was provided"

                    explanation = {
                        'error': _error,
                        'example': {
                            'use_semantic_types': fail['use_semantic_types'],
                            'use_index': fail['use_index'],
                            'categorial_targets': fail['categorial_targets']
                        }
                    }
                    explanations.append(explanation)
                    break

        if explanations:
            self.attributes['summary']['semantic_types'] = {
                'has_use_semantic_types': self.attributes['has_use_semantic_types'],
                'problems': explanations,
            }

    def check_descriptions(self):
        # check primitive description is not from base class
        primitive_description_base_class = None
        primitive_description = self.primitive.__doc__

        for base in self.primitive.__bases__:
            if base.__doc__ == primitive_description:
                primitive_description_base_class = base
                break

        description = {}
        if primitive_description_base_class is not None:
            description['primitive_description'] = \
                'Primitive description from class ' + str(primitive_description_base_class)

        seen_hyperparameter_descriptions = []
        hp_descriptions_problems = {}
        for hyperparameter, hyperparam_space in self.hyperparameter_space.configuration.items():
            hp_description = hyperparam_space.description

            # check if no description is provided
            if hp_description is None:
                hp_descriptions_problems[hyperparameter] = 'Description is None'
            # check if hp description matches with the class doc
            elif hp_description == type(hyperparam_space).__doc__:
                hp_descriptions_problems[hyperparameter] = 'Description from ' + str(type(hyperparam_space))
            # check if the description is from another hyperparameter.
            elif hp_description in seen_hyperparameter_descriptions:
                for hp, desc in self.hyperparameter_space.configuration.items():
                    if hp_description == desc and hyperparameter != hp:
                        hp_descriptions_problems[hyperparameter] = 'Description from hyperparameter' + hp
                        break
            # check if description matches with any docstring from base classes
            else:
                for base in type(hyperparam_space).__bases__:
                    if hp_description == base.__doc__:
                        hp_descriptions_problems[hyperparameter] = 'Description from ' + str(type(base))
                        break

            seen_hyperparameter_descriptions.append(hp_description)

        if hp_descriptions_problems:
            description['hyperparameters_description'] = hp_descriptions_problems

        if description:
            self.attributes['descriptions'] = description
            self.attributes['summary'] = {'descriptions': description}
